
    let firstNumber;
    let secondNumber;
    let mathSign;
    let result;

    function calcNumber(num1, num2, sign) {
      let result;
      switch (sign) {
        case '+':
          result = num1 + num2;
          break;
        case '-':
          result = num1 - num2;
          break;
        case '*':
          result = num1 * num2;
          break;
        case '/':
          result = num1 / num2;
          break;
        }
      return result;
    }

    function getSign() {
      return prompt('Введіть операцію (+,-,*,/)');
    }

    function getValue(text, value) {
      return prompt(`Enter ${text} number`, value);
    }

    function setSign() {
      let result;
      do {
        let sign = getSign();
        if (sign === null) {
          console.log('Помилка');
          break;
        }
        else if (sign == '+' || sign == '-' || sign == '*' || sign == '/') {
          result = sign;
        }
      } while (!result);
      return result;
    }

    function setNumber(text, num) {
      do {
        num = getValue(text, num);
        if (num === null) {
          console.log('ви не ввели число');
          break;
        }
        else if (num === '' || num[0] === ' ') {
          num = undefined;
        }
        else if (isFinite(num)) {
          num = Number(num);
        }
      } while (!isFinite(num));
      return num;
    }

    firstNumber = setNumber('first', firstNumber);

    if (Number.isFinite(firstNumber)) {
      secondNumber = setNumber('second', secondNumber);
    }
    if (Number.isFinite(secondNumber)) {
      mathSign = setSign();
    }

    if (Number.isFinite(firstNumber) && Number.isFinite(secondNumber)) {
      result = calcNumber(firstNumber, secondNumber, mathSign);
    }

    if (Number.isFinite(result)) {
      console.log(`Result: ${firstNumber} ${mathSign} ${secondNumber} = ${result}`);
    } else {
      console.log('помилка обчислення');
    }
